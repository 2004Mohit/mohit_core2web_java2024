class bool {
	public static void main(String[] s) {
		boolean x = true;
		boolean y = false;
		System.out.println("Logical AND : true && false : " +(x && y));
		System.out.println("Logical OR : true || false : " +(x || y));
		System.out.println("Logical NOT for the First Value : !true = false : " +(!x));
		System.out.println("Logical NOT for the Second Value : !false = true : " +(!y));
	}
}
