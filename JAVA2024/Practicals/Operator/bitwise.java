class bitwise {
	public static void main(String[] s) {
		int x = 5;
		int y = 3;
		System.out.println("Bitwise AND : 5 & 3 : "+(x & y));
		System.out.println("Bitwise OR : 5 | 3 : "+(x | y));
		System.out.println("Bitwise XOR : 5 ^ 3 : "+(x ^ y));
		System.out.println("Left Shift : 5 << 3 : "+(x << 1));
		System.out.println("Right Shift : 5 & 3 : "+(x >> 1));
	}
}
